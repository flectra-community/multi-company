{
    "name": "Inter Company Invoices Queued",
    "summary": "Generate invoices using jobs",
    "version": "2.0.1.0.1",
    "category": "Accounting & Finance",
    "website": "https://gitlab.com/flectra-community/multi-company",
    "author": "Ilyas, Ooops404, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "depends": ["account_invoice_inter_company", "queue_job"],
    "data": ["views/res_config_settings_view.xml"],
    "installable": True,
}
