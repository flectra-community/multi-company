# Flectra Community / multi-company

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[intercompany_shared_contact](intercompany_shared_contact/) | 2.0.1.1.2| User of each company are contact of a company partner.All child address of a company are automatically shared
[product_multi_company](product_multi_company/) | 2.0.1.0.1| Select individually the product template visibility on each company
[account_move_change_company](account_move_change_company/) | 2.0.1.0.2|         Allow to change company of account moves
[mail_multicompany](mail_multicompany/) | 2.0.0.1.0| Email Gateway Multi company
[account_invoice_consolidated](account_invoice_consolidated/) | 2.0.1.0.4| Consolidate your invoices across companies
[product_supplierinfo_group_intercompany](product_supplierinfo_group_intercompany/) | 2.0.1.1.3|         Add sequence field on grouped pricelist items
[account_payment_other_company](account_payment_other_company/) | 2.0.1.0.2| Create Payments for Other Companies
[company_dependent_attribute](company_dependent_attribute/) | 2.0.1.0.0| Display company dependent attribute on fields
[product_tax_multicompany_default](product_tax_multicompany_default/) | 2.0.1.2.0| Product Tax Multi Company Default
[partner_contact_company_propagation](partner_contact_company_propagation/) | 2.0.1.0.0| Propagate company info to children contacts
[mail_template_multi_company](mail_template_multi_company/) | 2.0.1.0.0| Mail Template Multi Company
[purchase_quick_intercompany](purchase_quick_intercompany/) | 2.0.0.1.1| Purchase Quick Intercompany
[partner_multi_company](partner_multi_company/) | 2.0.1.0.1| Select individually the partner visibility on each company
[product_category_inter_company](product_category_inter_company/) | 2.0.1.1.0| Product categories as company dependent
[purchase_sale_inter_company](purchase_sale_inter_company/) | 2.0.1.1.0| Intercompany PO/SO rules
[login_all_company](login_all_company/) | 2.0.1.0.1|         Access all your companies when you log in
[product_supplierinfo_intercompany](product_supplierinfo_intercompany/) | 2.0.1.1.1| Product SupplierInfo Intercompany
[account_multicompany_easy_creation](account_multicompany_easy_creation/) | 2.0.1.0.2| This module adds a wizard to create companies easily
[res_company_code](res_company_code/) | 2.0.1.0.1| Add 'code' field on company model
[base_multi_company](base_multi_company/) | 2.0.1.1.3| Provides a base for adding multi-company support to models.
[account_invoice_inter_company](account_invoice_inter_company/) | 2.0.1.4.0| Intercompany invoice rules
[account_invoice_inter_company_sale](account_invoice_inter_company_sale/) | 2.0.1.0.0| Show sale related fields
[account_invoice_inter_company_queued](account_invoice_inter_company_queued/) | 2.0.1.0.1| Generate invoices using jobs
[stock_intercompany](stock_intercompany/) | 2.0.1.1.1| Stock Intercompany Delivery-Reception
[product_supplierinfo_intercompany_multi_company](product_supplierinfo_intercompany_multi_company/) | 2.0.1.1.0|     Compatibility of product_multi_company and product_supplierinfo_intercompany
[multicompany_configuration](multicompany_configuration/) | 2.0.1.0.0|         Simplify the configuration on multicompany environments


