# Copyright (C) 2019 Open Source Integrators
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Account Payment Other Company",
    "version": "2.0.1.0.2",
    "summary": "Create Payments for Other Companies",
    "author": "Open Source Integrators, " "Odoo Community Association (OCA)",
    "category": "Invoicing Management",
    "website": "https://gitlab.com/flectra-community/multi-company",
    "license": "AGPL-3",
    "depends": ["account_invoice_consolidated"],
    "data": ["views/account_payment.xml", "wizard/account_register_payments.xml"],
    "development_status": "Beta",
    "maintainers": ["max3903", "osi-scampbell"],
}
