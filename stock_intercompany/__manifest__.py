# Copyright 2021 Camptocamp
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    "name": "Stock Intercompany Delivery-Reception",
    "Summary": "Module that adds possibility for intercompany Delivery-Reception",
    "version": "2.0.1.1.1",
    "author": "Camptocamp, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/multi-company",
    "category": "Warehouse Management",
    "depends": ["stock"],
    "installable": True,
    "license": "AGPL-3",
    "data": [
        "views/res_config_settings.xml",
    ],
}
