{
    "name": "Inter Company Invoices Sale",
    "summary": "Show sale related fields",
    "version": "2.0.1.0.0",
    "category": "Accounting & Finance",
    "website": "https://gitlab.com/flectra-community/multi-company",
    "author": "Ilyas, Ooops404, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "depends": ["account_invoice_inter_company", "sale"],
    "data": ["views/account_move_views.xml"],
    "installable": True,
}
