# Copyright 2022 CreuBlanca
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
    "name": "Account Change Company",
    "summary": """
        Allow to change company of account moves""",
    "version": "2.0.1.0.2",
    "website": "https://gitlab.com/flectra-community/multi-company",
    "license": "AGPL-3",
    "author": "CreuBlanca,Odoo Community Association (OCA)",
    "depends": ["account"],
    "data": [
        "views/account_move.xml",
    ],
    "demo": [],
}
