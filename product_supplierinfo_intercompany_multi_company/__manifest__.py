{
    "name": "Product SupplierInfo Intercompany Multi Company",
    "summary": """
    Compatibility of product_multi_company and product_supplierinfo_intercompany""",
    "version": "2.0.1.1.0",
    "category": "Generic Modules/Others",
    "license": "AGPL-3",
    "author": "Ilyas, Ooops404, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/multi-company",
    "depends": [
        "product_supplierinfo_intercompany",
        "product_multi_company",
    ],
    "installable": True,
    "auto_install": True,
}
