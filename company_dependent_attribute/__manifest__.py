# © 2019 David BEAL @ Akretion
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    "name": "Company Dependent Attribute",
    "summary": "Display company dependent attribute on fields",
    "version": "2.0.1.0.0",
    "category": "Tools",
    "author": "Akretion, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/multi-company",
    "license": "AGPL-3",
    "depends": ["base"],
    "data": ["views/field_view.xml"],
    "installable": True,
}
